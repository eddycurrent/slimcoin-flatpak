if [ ! -d "Slimcoin" ]; then
	git clone https://github.com/slimcoin-project/Slimcoin.git
else
	cd Slimcoin
	git pull
	cd ..
fi
cd Slimcoin
qmake USE_DBUS=1
make -j$(nproc)
